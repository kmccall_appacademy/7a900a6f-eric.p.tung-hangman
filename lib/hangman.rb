require 'byebug'

class Hangman
  attr_reader :guesser, :referee, :board, :incorrect_guesses

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @incorrect_guesses = []
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    @board = Array.new(secret_word_length)
    @guesser.register_secret_length(secret_word_length)
  end

  def take_turn
    display
    puts "Make your guess!"
    letter = @guesser.guess(@board)
    indices = @referee.check_guess(letter)
    @incorrect_guesses << letter if indices.empty?
    update_board(letter, indices)
    display
    @guesser.handle_response(letter, indices)
  end

  def update_board(letter, indices)
    indices.each { |idx| @board[idx] = letter }
  end

  def display
    display_graphics
    puts ""
    display_board
    puts ""
    puts "Wrong guesses: #{@incorrect_guesses}"
  end

  def play
    setup
    until game_over?
      take_turn
    end
  end

  def game_over?
    if won?
      puts "Congrats Guesser, you guessed the secret word!"
      puts "The Secret word was #{@referee.secret_word.upcase}" unless @referee.secret_word.nil?
      return true
    end
    if lost?
      puts "You failed to guess the secret word and you have been hung. Better luck in the next life."
      puts "The Secret word was #{@referee.secret_word.upcase}" unless @referee.secret_word.nil?
      return true
    end
    false
  end

  def won?
    return false if @board.any? { |el| el.nil? }
    true
  end

  def lost?
    return true if incorrect_guesses.length > 4
    false
  end


  def display_graphics
    graphic = []
    graphic[0]="   _____  "
    graphic[1]="  |     | "
    graphic[2]="        | "
    graphic[3]="        | "
    graphic[4]="        | "
    graphic[5]="  ______|_"
    if @incorrect_guesses.length > 0
      graphic[2][1]="("
      graphic[2][3]=")"
      graphic[3][2]="|"
      if @incorrect_guesses.length > 1
        graphic[4][3]="\\"
        if @incorrect_guesses.length > 2
          graphic[4][1]="/"
          if @incorrect_guesses.length > 3
            graphic[3][1]="-"
            if @incorrect_guesses.length > 4
              graphic[3][3]="-"
            end
          end
        end
      end
    end
    graphic.each { |row| puts row }
  end

  def display_board
    @board.each do |el|
      if el.nil?
        print "_"
      else
        print el
      end
      print " "
    end
  end

end

class HumanPlayer

  ALPHABET = "abcdefghijklmnopqrstuvwxyz"

  attr_reader :secret_word
  def initialize
  end

  def register_secret_length(secret_word_length)
    puts "The length of the secret word is: #{secret_word_length}"
  end

  def pick_secret_word
    puts "CHOOSE YOUR SECRET WORD! Tell me how many letters are in it!"
    num_letters = gets.chomp.to_i
  end

  def guess(board)
    puts "Guess a letter!"
    successful_guess = false
    while successful_guess == false
      letter = gets.chomp
      successful_guess = true if valid_guess?(letter)
    end
    letter
  end

  def check_guess(letter)
    puts "Is the letter #{letter} in your secret word?"
    puts "If yes, provide the indices the letter is in (separate with commas), otherwise say 'NO'"
    ans = gets.chomp
    if ans == "NO"
      return []
    else
      return ans.split(",").map { |el| el.to_i }
    end
  end

  def handle_response(letter, indices)
    if indices.length.zero?
      puts "Sorry, that letter is not in the secret word!"
    else
      puts "Congrats! The letter #{letter} is found in the positions #{indices}!"
    end
  end

  private

  def valid_guess?(letter)
    ALPHABET.include?(letter)
  end
end

class ComputerPlayer

  ALPHABET = "abcdefghijklmnopqrstuvwxyz"

  attr_reader :dictionary, :secret_word, :candidate_words

  def initialize(dictionary = "dictionary.txt")
    if dictionary == "dictionary.txt"
      @dictionary = File.readlines(dictionary)
    else
      @dictionary = dictionary
    end
    @candidate_words = []
    @incorrect_guesses = []
  end

  def register_secret_length(secret_word_length)
    @candidate_words = []
    @dictionary.each do |word|
      word.chop! if word[-1] == "\n"
      @candidate_words << word if word.length == secret_word_length
    end
    @candidate_words
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.chop! if @secret_word[-1] == "\n"
    @secret_word.length
  end

  def check_guess(guessed_letter)
    indices = []
    @secret_word.chars.each_with_index { |letter, idx| indices << idx if letter == guessed_letter}
    indices
  end

  def guess(board)
    # register_secret_length(board.length)
    update_responses(board)
    words_with_letters=Hash.new(0)
    @candidate_words.each do |word|
      ALPHABET.chars.each { |alpha| words_with_letters[alpha]+=1 if word.include?(alpha) }
    end
    words_with_letters.sort_by { |k, v| v }.reverse.each { |key, val| return key if !board.include?(key) }
  end

  def update_responses(board)
    board_hash = Hash.new{ |board_hash, k| board_hash[k] = [] }
    board.each_with_index do |letter, idx|
      board_hash[letter] << idx unless letter.nil?
    end
    board_hash.each { |k, v| handle_response(k, v) }
  end

  def valid_word?(word, letter, indices)
    idx_of_word = (0...word.length).to_a.reject { |el| indices.include?(el) }
    if indices.all? { |idx| word[idx]==letter }
      if idx_of_word.any? { |idx| word[idx] == letter }
        return false
      else
        return true
      end
    else
      return false
    end
  end

  def handle_response(letter, indices)
    new_candidate_words = []
    if indices.empty?
      @candidate_words.reject! { |word| word.include?(letter) }
    else
      @candidate_words.select! { |word| valid_word?(word, letter, indices) }
    end
    @candidate_words
  end

end
